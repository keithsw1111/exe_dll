//(*AppHeaders
#include <wx/image.h>
//*)

#include "dllApp.h"
#include "log.h"
#include "ModelessDialog.h"
#include <wx/wx.h>
#include <sstream>
#include <atomic>

#include <wx/msw/private.h>
#ifdef __WXMSW__
HANDLE __dllThread = 0;
HANDLE _hModule = 0;
bool _wasNullhInstance = false;
#endif

std::atomic<bool> _started = false;

IMPLEMENT_APP_NO_MAIN(dllApp)

#ifdef __WXMSW__
DWORD WINAPI ThreadProc(LPVOID lpParameter)
#else
    // need appropriate function header here
#endif
{
    log("dll.dll                in thread\n");

    wxInitialize();

    wxApp::SetInstance(new dllApp());

    _started = true;

#ifdef __WXMSW__
    wxEntry((HINSTANCE)_hModule, NULL, NULL, SW_SHOW);
#else
    // need to call the right platform version of wxEntry
#endif

    wxUninitialize();

    return true;
}

ModelessDialog* __dialog = nullptr;

extern "C" {

    bool WXEXPORT dll_Load()
    {
        log("dll.dll        dll_Load reached\n");

        if (__dialog != nullptr) return true;

        if (_wasNullhInstance)
        {
            if (__dllThread != 0) return false;

            _started = false;

            // Must use native thread creation here
#ifdef __WXMSW__
            __dllThread = ::CreateThread(NULL, 0, ThreadProc, NULL, 0, NULL);
#else
            // need to add platform specific thread creation for other platforms
#endif
            while (!_started)
            {
                wxMilliSleep(10);
            }
        }

        if (!_wasNullhInstance)
        {
            __dialog = new ModelessDialog(nullptr);
            __dialog->Show();
        }
        return true;
    }

    void WXEXPORT dll_Unload()
    {
        log("dll.dll        dll_Unload reached\n");

        if (__dialog == nullptr) return;

        __dialog->Close(true);

        if (_wasNullhInstance)
        {
            wxEntryCleanup();
        }
        else 
        {
            delete __dialog;
        }
        __dialog = nullptr;

#ifdef __WXMSW__
        if (__dllThread == 0) return;
        ::WaitForSingleObject(__dllThread, INFINITE);
        __dllThread = 0;
#else
        // need to add code to wait for thread to exit for other operating systems
#endif
    }
}

int dllApp::OnExit()
{
    log("dll.dll        dll OnExit()\n");
    return 0;
}

bool dllApp::OnInit()
{
    log("dll.dll        dll OnInit\n");

    if (_wasNullhInstance)
    {
        __dialog = new ModelessDialog(nullptr);
        __dialog->Show();
    }

    return true;
}

#ifdef __WXMSW__
extern "C"
{
    BOOL APIENTRY DllMain(HANDLE hModule,
        DWORD  ul_reason_for_call, LPVOID lpReserved)
    {
        switch (ul_reason_for_call)
        {
        case DLL_PROCESS_ATTACH:
            log("dll.dll        dll process attach\n");
            _wasNullhInstance = (wxTheApp == 0);
            wxSetInstance((HINSTANCE)hModule);
            _hModule = hModule; // save the hmodule to use when we call wxentry
            break;
        case DLL_THREAD_ATTACH: break;
        case DLL_THREAD_DETACH: break;
        case DLL_PROCESS_DETACH:
            log("dll.dll        dll process detach\n");
            break;
        }

        return TRUE;
    }
}
#endif
