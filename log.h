#ifndef log_H
#define log_H

void inline log(const std::string& s)
{
	FILE* f = fopen("log.txt", "a");
	if (f != nullptr)
	{
		fputs((const char*)s.c_str(), f);
        fclose(f);
    }
}

void inline clearlog()
{
	FILE* f = fopen("log.txt", "w");
    if (f != nullptr) fclose(f);
}

#endif 
