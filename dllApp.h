#ifndef dllAPP_H
#define dllAPP_H

#include <wx/app.h>

class dllApp : public wxApp
{
    public:
        virtual bool OnInit() override;
        virtual int OnExit() override;
};

DECLARE_APP(dllApp)

#endif // dllAPP_H
