#ifndef exeMAIN_H
#define exeMAIN_H

#ifdef _MSC_VER
#include <stdlib.h>
#endif

//(*Headers(exeFrame)
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/panel.h>
#include <wx/sizer.h>
//*)

#include <list>

class wxDynamicLibrary;

class exeFrame : public wxFrame
{
    wxDynamicLibrary* _dl = nullptr;

    void DoLoad();
    void DoUnload();

public:

        exeFrame(wxWindow* parent, const std::string& showdir = "", const std::string& playlist = "", wxWindowID id = -1);
        virtual ~exeFrame();

    private:

        //(*Handlers(exeFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnResize(wxSizeEvent& event);
        void OnButton1Click(wxCommandEvent& event);
        void OnButton2Click(wxCommandEvent& event);
        //*)
        //(*Identifiers(exeFrame)
        static const long ID_BUTTON1;
        static const long ID_BUTTON2;
        static const long ID_PANEL2;
        //*)

        //(*Declarations(exeFrame)
        wxButton* Button1;
        wxButton* Button2;
        wxFlexGridSizer* FlexGridSizer1;
        wxPanel* Panel2;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // exeMAIN_H
