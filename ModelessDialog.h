#ifndef MODELESSDIALOG_H
#define MODELESSDIALOG_H

//(*Headers(ModelessDialog)
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
//*)

class ModelessDialog: public wxDialog
{
	public:

		ModelessDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~ModelessDialog();

		//(*Declarations(ModelessDialog)
		wxButton* Button1;
		wxStaticText* StaticText1;
		//*)

	protected:

		//(*Identifiers(ModelessDialog)
		static const long ID_STATICTEXT1;
		static const long ID_BUTTON1;
		//*)

	private:

		//(*Handlers(ModelessDialog)
		void OnButton1Click(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
