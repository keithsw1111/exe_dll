#ifndef exeAPP_H
#define exeAPP_H

#ifdef _MSC_VER
#include <stdlib.h>
#endif

#include <wx/app.h>

class exeApp : public wxApp
{
    public:
        virtual bool OnInit() override;
        virtual int OnExit() override;
};

DECLARE_APP(exeApp)

#endif // exeAPP_H
