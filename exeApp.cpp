#include "exeApp.h"
#include "log.h"

//(*AppHeaders
#include "exeMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(exeApp)

int exeApp::OnExit()
{
    log("exe.exe OnExit\n\n\n");
    return 0;
}

bool exeApp::OnInit()
{
    log("\n\n\n\n\nexe.exe OnInit()\n");

    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	exeFrame* Frame = new exeFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;
}