#define ZERO 0

//(*InternalHeaders(exeFrame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

#include "exeMain.h"
#include "log.h"
#include <wx/dynlib.h>

//(*IdInit(exeFrame)
const long exeFrame::ID_BUTTON1 = wxNewId();
const long exeFrame::ID_BUTTON2 = wxNewId();
const long exeFrame::ID_PANEL2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(exeFrame,wxFrame)
    //(*EventTable(exeFrame)
    //*)
END_EVENT_TABLE()

bool WXIMPORT dll_Load();
typedef bool (*p_dll_Load)();

bool WXIMPORT dll_Unload();
typedef bool (*p_dll_Unload)();

void exeFrame::DoLoad()
{
    if (_dl != nullptr) return;

    log("exe.exe DoLoad.\n");

    _dl = new wxDynamicLibrary();
    _dl->Load("dll.dll");

    if (_dl->IsLoaded())
    {
        log("exe.exe dll.dll loaded.\n");
        p_dll_Load fn = (p_dll_Load)_dl->GetSymbol("dll_Load");

        if (fn != nullptr)
        {
            log("exe.exe dll.dll dll_Load found.\n");
            fn();
        }
        else
        {
            log("exe.exe dll.dll dll_Load NOT found.\n");
            delete _dl;
            _dl = nullptr;
        }
    }
    else
    {
        log("exe.exe dll.dll NOT loaded.\n");
        delete _dl;
        _dl = nullptr;
    }
}

void exeFrame::DoUnload()
{
    if (_dl == nullptr) return;

    log("exe.exe DoUnload.\n");

    p_dll_Unload fn = (p_dll_Unload)_dl->GetSymbol("dll_Unload");

    if (fn != nullptr)
    {
        log("exe.exe dll.dll dll_Unload found.\n");
        fn();
        delete _dl;
        _dl = nullptr;
    }
    else
    {
        log("exe.exe dll.dll dll_Unload NOT found.\n");
    }
}

exeFrame::exeFrame(wxWindow* parent, const std::string& showdir, const std::string& playlist, wxWindowID id)
{
    log("exe.exe exeframe constructor\n");

    //(*Initialize(exeFrame)
    wxFlexGridSizer* FlexGridSizer3;

    Create(parent, id, _("xSchedule"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    FlexGridSizer1 = new wxFlexGridSizer(0, 1, 0, 0);
    FlexGridSizer1->AddGrowableRow(0);
    Panel2 = new wxPanel(this, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxRAISED_BORDER|wxTAB_TRAVERSAL, _T("ID_PANEL2"));
    FlexGridSizer3 = new wxFlexGridSizer(0, 2, 0, 0);
    FlexGridSizer3->AddGrowableRow(0);
    Button1 = new wxButton(Panel2, ID_BUTTON1, _("Load"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    FlexGridSizer3->Add(Button1, 1, wxALL|wxEXPAND, 5);
    Button2 = new wxButton(Panel2, ID_BUTTON2, _("Unload"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
    FlexGridSizer3->Add(Button2, 1, wxALL|wxEXPAND, 5);
    Panel2->SetSizer(FlexGridSizer3);
    FlexGridSizer3->Fit(Panel2);
    FlexGridSizer3->SetSizeHints(Panel2);
    FlexGridSizer1->Add(Panel2, 1, wxALL|wxEXPAND, 0);
    SetSizer(FlexGridSizer1);
    FlexGridSizer1->Fit(this);
    FlexGridSizer1->SetSizeHints(this);

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&exeFrame::OnButton1Click);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&exeFrame::OnButton2Click);
    //*)

    SetTitle("exe");

    char buf[100];
    sprintf(buf, "exeFrame HWND 0x%llx\n", (uint64_t)GetHWND());
    log(buf);
}

exeFrame::~exeFrame()
{
    char buf[100];
    sprintf(buf, "~exeFrame HWND 0x%llx\n", (uint64_t)GetHWND());
    log(buf);

    log("~exeFrame about to DoUnload\n");
    DoUnload();
    log("~exeFrame DoUnload done\n");
}

void exeFrame::OnQuit(wxCommandEvent& event)
{
    log("exeFrame::OnQuit\n");
    Close();
}

void exeFrame::OnAbout(wxCommandEvent& event)
{
}

void exeFrame::OnResize(wxSizeEvent& event)
{
}

void exeFrame::OnButton1Click(wxCommandEvent& event)
{
    log("exe.exe exeframe User pressed load button.\n");
    DoLoad();
}

void exeFrame::OnButton2Click(wxCommandEvent& event)
{
    log("exe.exe exeframe User pressed unload button.\n");
    DoUnload();
}
