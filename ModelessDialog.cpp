#include "ModelessDialog.h"

//(*InternalHeaders(ModelessDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)
#include <wx/msgdlg.h>

#include "log.h"

//(*IdInit(ModelessDialog)
const long ModelessDialog::ID_STATICTEXT1 = wxNewId();
const long ModelessDialog::ID_BUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ModelessDialog,wxDialog)
	//(*EventTable(ModelessDialog)
	//*)
END_EVENT_TABLE()

ModelessDialog::ModelessDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(ModelessDialog)
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxCAPTION|wxRESIZE_BORDER|wxMAXIMIZE_BOX|wxMINIMIZE_BOX, _T("id"));
	SetClientSize(wxDefaultSize);
	Move(wxDefaultPosition);
	FlexGridSizer1 = new wxFlexGridSizer(0, 1, 0, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Modeless sucess!!"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxEXPAND, 5);
	Button1 = new wxButton(this, ID_BUTTON1, _("Say Hi"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	FlexGridSizer1->Add(Button1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ModelessDialog::OnButton1Click);
	//*)

    char buf[100];
    sprintf(buf, "Modeless HWND 0x%llx\n", (uint64_t)GetHWND());
    log(buf);
}

ModelessDialog::~ModelessDialog()
{
    char buf[100];
    sprintf(buf, "~ModelessDialog HWND 0x%llx\n", (uint64_t)GetHWND());
    log(buf);
    //(*Destroy(ModelessDialog)
	//*)
}


void ModelessDialog::OnButton1Click(wxCommandEvent& event)
{
    log("Say hi");
    wxMessageBox("Hi");
}
